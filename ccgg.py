#!/usr/bin/python

# Create Class Google Groups
# This script will take the class list of students for the district and create Google Groups by teacher-period 
# based on their 4 character teacher id in DASL

from sys import argv
from subprocess import Popen, PIPE, call
from datetime import date
import csv, shlex, codecs, re, argparse

# Figure out current term
endof1st = "2014-01-16"
currentterm = '1SEM' if date.today().isoformat() <= endof1st else '2SEM'
domain = "kentoncityschools.org"
gampy = "/usr/bin/python /home/kadmin/bin/gam/gam.py"

# Functions
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--debug", help="Dry run, nothing will be changed", action="store_true")
args = parser.parse_args()

if args.debug:
    print "Debug mode is on, nothing will change in this run."

def updateGroup(g,e,type='student'):
    # Requires google-apps-manager
    global classgroups,groupmembers,domain,args
    print "Updating " + g
    # print groupmembers[g]
    if not g in classgroups:
        if not args.debug:
            out = gam("create group " + g + " name \"Class Google Groups - " + g + "\" description \"Created by ccgg\"")
            out = gam("update group " + g + "@" + domain + " settings who_can_view_membership all_members_can_view")
            out = gam("update group " + g + "@" + domain + " settings reply_to reply_to_sender")
            out = gam("update group " + g + "@" + domain + " settings who_can_post_message all_in_domain_can_post")
        else:
            print "Creating " + g
        classgroups.append(g)
        groupmembers[g] = []
    
    #Add email to group
    if not e in groupmembers[g]:
        if not args.debug:
            out = gam("update group " + g + " add member " + e)
            if type == 'teacher':
                out = gam("update group " + g + " add owner " + e)
        else:
            print "Adding " + e + " to group " +g
def gam(args):
    global gampy
    cmd = shlex.split(gampy + " " + args)
    stdout,stderr = Popen(cmd,stdout=PIPE).communicate()
    return stdout.splitlines()

# Get current class groups
cgl_csv = gam("print groups name description")
classgroupslist = csv.DictReader(cgl_csv)

classgroups = []
groupmembers = {}

print '\nGetting current group information...'
for row in classgroupslist:
    if row['Description'] == 'Created by ccgg':
        print row
        group,domain = row['Email'].split("@")
        print(group + ", "),
        #Get group members
        members = []
        groupinfo = gam("info group " + group)
        for line in groupinfo:
            if line[1:8] == "member:":
                member = re.search(r" member: (.*) \(user\)",line)
                members.append(member.group(1))
            elif line[1:7] == "owner:":
                member = re.search(r" owner: (.*) \(user\)",line)
                members.append(member.group(1))
        
        groupmembers[group] = members
        classgroups.append(group)

schedule_csv = codecs.open("StudentSchedules.csv",'r',encoding='utf-8-sig')
studentschedules = csv.DictReader(schedule_csv,delimiter='	')

print "\nImporting StudentSchedules.csv..."
newclassgroups = []
newgroupmembers = {}
teachers = {}

for row in studentschedules:
    studentemail = row['studentemail']
    staffemail = row['staffemail']
    staff = re.sub(r'\W+','',row['teacher_code'].lower())  
    term = row['Term']
    period = row['Class Period'].lower()
    teachers[staff] = staffemail    

    group = staff + "-" + period
    
    if ((term == currentterm or term == 'YEAR') and staff.__len__() == 4):
        #Class period groups
        if not group in newclassgroups:  
            newclassgroups.append(group)
        if not newgroupmembers.has_key(group):
            newgroupmembers[group] = [studentemail]
        else:
            newgroupmembers[group].append(studentemail)
        print "Managing " + studentemail + " in group " + group + " for " + term
        updateGroup(group,studentemail)

        # Staff all students
        if not staff in newclassgroups:  
            newclassgroups.append(staff)
        if not newgroupmembers.has_key(staff):
            newgroupmembers[staff] = [studentemail]
        else:
            newgroupmembers[staff].append(studentemail)

        print "Managing " + studentemail + " in group " + group + " for " + term
        updateGroup(staff,studentemail)
        
schedule_csv.close() 

for classgroup in newclassgroups:
    c = classgroup[:4]
    print "Setting " + teachers[c] + " owner of " + classgroup + "@" + domain
    if not teachers[c] in groupmembers[classgroup]:
        updateGroup(classgroup,teachers[c],'teacher')
    newgroupmembers[classgroup].append(teachers[c])

# Remove students that aren't in the current import file
print "Removing inactive students..."

for e in classgroups:
    print(e + ','),
    for u in groupmembers[e]:
        try:
            if not u in newgroupmembers[e]:
                print "\nRemoving " + u + " from " + e
                print "update group " + e + "@" + domain + " remove user " + u
                if not args.debug:
                    out = gam("update group " + e + "@" + domain + " remove user " + u)
        except:
            print "Error with group " + e + " or " + u 

    if not e in newclassgroups:
       print "Need to delete  group " + e
       with open("groups2delete.csv","a") as o:
           o.write(e + "@" + domain + "\n")

