# Create Class Google Groups

This script will take a tab delimited schedule file with email addresses and create Google Groups for each class. It will also create one Google Group that contains all the students of a particular teacher. You can use -d or --debug to do a dry run of the script

# Import file

The import file is a tab delimited file with the entire district schedule in it. Depending on how your terms are named, you may need to change the source to match. There is a sample import file. The teacher_code field is used for the name of the Google Group, so make sure it is not the same name as the teacher. Our schedule export file has a unique 4 character code that we use.

# Instructions

You must have [Google Apps Manager 3.0](http://code.google.com/p/google-apps-manager/) installed and working. At the beginning of the ccgg.py source file, enter the location of gam.py and set your domain. Then run the script and see what happens. :-) Each group will have its description set to **Created by ccgg**. Don't change the description or ccgg will stop managing the group.

You can use a small schedule list at first to make sure the script is going to do what you think it's going to do. If there are any groups to be deleted, a groups2delete.csv file will be created. Under Linux, you can feed this file to gam.py with 

    while read group; do gam.py delete group ${group}; done < groups2delete.csv

Each run appends to this file, so there may be duplicates. 

There is hardly any error checking, use at your own risk... :-)


